﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using PipeAPI.Models;

namespace PipeAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CmlController : ControllerBase
    {
        private DbCoreContext db = new DbCoreContext();

        [HttpGet]
        public IActionResult GetCml()
        {
            List<Cml> CMLs = db.Cml.ToList();
            return Ok(CMLs);
        }

        [HttpPost]
        [Route("[action]")]
        public IActionResult GetCmlByInfo([FromBody] Cml cml)
        {
            List<Cml> CMLs = db.Cml.Where(field => field.line_number == cml.line_number).ToList();
            return Ok(CMLs);
        }

        [HttpPost]
        public IActionResult PostCml([FromBody] Cml cml)
        {
            if (cml.line_number.IsNullOrEmpty() || cml.cml_number <= 0)
            {
                return BadRequest();
            }

            try
            {
                Cml cmlModel = new Cml();
                cmlModel.line_number = cml.line_number;
                cmlModel.cml_number = cml.cml_number;
                cmlModel.cml_description = cml.cml_description;
                cmlModel.actual_outside_diameter = cml.actual_outside_diameter;
                cmlModel.design_thickness = cml.design_thickness;
                cmlModel.structural_thickness = cml.structural_thickness;
                cmlModel.required_thickness = cml.required_thickness;

                db.Cml.Add(cmlModel);
                db.SaveChanges();

                return Ok(200);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }

        [HttpPatch]
        public IActionResult PatchCml([FromBody] Cml cml)
        {
            try
            {
                Cml cmlModel = db.Cml.FirstOrDefault(
                    field => field.line_number == cml.line_number && 
                    field.cml_number == cml.cml_number);

                cmlModel.cml_description = cml.cml_description;
                cmlModel.actual_outside_diameter = cml.actual_outside_diameter;
                cmlModel.design_thickness = cml.design_thickness;
                cmlModel.structural_thickness = cml.structural_thickness;
                cmlModel.required_thickness = cml.required_thickness;

                db.SaveChanges();

                return Ok(200);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }

        [HttpDelete]
        public async Task<IActionResult> DeleteCml([FromBody] Cml cml)
        {
            try
            {
                if (db.Thickness.Where((field => field.cml_number == cml.cml_number)).Count() > 0)
                {
                    await db.Thickness.Where(field => field.cml_number == cml.cml_number).ExecuteDeleteAsync();
                }
                if (db.TestPoint.Where((field => field.cml_number == cml.cml_number)).Count() > 0)
                {
                    await db.TestPoint.Where(field => field.cml_number == cml.cml_number).ExecuteDeleteAsync();
                }
                await db.Cml.Where(field => field.line_number == cml.line_number &&
                                    field.cml_number == cml.cml_number)
                                    .ExecuteDeleteAsync();

                db.SaveChanges();

                return Ok(200);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }
    }
}
