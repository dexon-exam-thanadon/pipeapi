﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using PipeAPI.Models;

namespace PipeAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ThicknessController : ControllerBase
    {
        private DbCoreContext db = new DbCoreContext();

        [HttpGet]
        public IActionResult GetThickness()
        {
            List<Thickness> thicknesses = db.Thickness.ToList();
            return Ok(thicknesses);
        }

        [HttpPost]
        [Route("[action]")]
        public IActionResult GetThicknessByTp([FromBody] Thickness tk)
        {
            List<Thickness> thicknesses = db.Thickness.Where(
                field => field.line_number == tk.line_number &&
                field.cml_number == tk.cml_number &&
                field.tp_number == tk.tp_number).ToList();
            return Ok(thicknesses);
        }

        [HttpPost]
        public IActionResult PostThickness([FromBody] Thickness tk)
        {
            try
            {
                Thickness thicknessModel = new Thickness();
                thicknessModel.line_number = tk.line_number;
                thicknessModel.cml_number = tk.cml_number;
                thicknessModel.tp_number = tk.tp_number;
                thicknessModel.inspection_date = tk.inspection_date;
                thicknessModel.actual_thickness = tk.actual_thickness;

                db.Thickness.Add(thicknessModel);
                db.SaveChanges();

                return Ok(200);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }

        [HttpPatch]
        public IActionResult PatchThickness([FromBody] Thickness tk)
        {
            if (tk.line_number.IsNullOrEmpty() || tk.cml_number <= 0 || 
                tk.tp_number <= 0 || tk.inspection_date.ToString().IsNullOrEmpty())
            {
                return BadRequest();
            }

            try
            {
                Thickness thicknessModel = db.Thickness.FirstOrDefault(
                    field => field.line_number == tk.line_number &&
                field.cml_number == tk.cml_number &&
                field.tp_number == tk.tp_number &&
                field.inspection_date == tk.inspection_date);

                thicknessModel.inspection_date = tk.inspection_date;
                thicknessModel.actual_thickness = tk.actual_thickness;

                db.SaveChanges();

                return Ok(200);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }

        [HttpDelete]
        public async Task<IActionResult> DeleteThickness([FromBody] Thickness tk)
        {
            try
            {
                await db.Thickness.Where(field => field.line_number == tk.line_number &&
                                        field.cml_number == tk.cml_number &&
                                        field.tp_number == tk.tp_number &&
                                        field.inspection_date == tk.inspection_date)
                                        .ExecuteDeleteAsync();

                db.SaveChanges();

                return Ok(200);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }
    }
}
