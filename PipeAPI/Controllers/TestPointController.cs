﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using PipeAPI.Models;

namespace PipeAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestPointController : ControllerBase
    {
        private DbCoreContext db = new DbCoreContext();

        [HttpGet]
        public IActionResult GetTestPoint()
        {
            List<TestPoint> testPoints = db.TestPoint.ToList();
            return Ok(testPoints);
        }

        [HttpPost]
        [Route("[action]")]
        public IActionResult GetTestPointByCml([FromBody] TestPoint tp)
        {
            List<TestPoint> testPoints = db.TestPoint.Where(
                field => field.line_number == tp.line_number &&
                field.cml_number == tp.cml_number).ToList();
            return Ok(testPoints);
        }

        [HttpPost]
        public IActionResult PostTestPoint([FromBody] TestPoint tp)
        {
            if (tp.line_number.IsNullOrEmpty() || tp.cml_number <= 0 || tp.tp_number <= 0)
            {
                return BadRequest();
            }

            try
            {
                TestPoint testPointModel = new TestPoint();
                testPointModel.line_number = tp.line_number;
                testPointModel.cml_number = tp.cml_number;
                testPointModel.tp_number = tp.tp_number;
                testPointModel.tp_description = tp.tp_description;
                testPointModel.note = tp.note;

                db.TestPoint.Add(testPointModel);
                db.SaveChanges();

                return Ok(200);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }

        [HttpPatch]
        public IActionResult PatchTestPoint([FromBody] TestPoint tp)
        {
            try
            {
                TestPoint testPointModel = db.TestPoint.FirstOrDefault(
                    field => field.line_number == tp.line_number &&
                field.cml_number == tp.cml_number &&
                field.tp_number == tp.tp_number);

                testPointModel.tp_description = tp.tp_description;
                testPointModel.note = tp.note;

                db.SaveChanges();

                return Ok(200);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }

        [HttpDelete]
        public async Task<IActionResult> DeleteTestPoint([FromBody] TestPoint tp)
        {
            try
            {
                if (db.Thickness.Where((field => field.tp_number == tp.tp_number)).Count() > 0)
                {
                    await db.Thickness.Where(field => field.tp_number == tp.tp_number).ExecuteDeleteAsync();
                }

                await db.TestPoint.Where(field => field.line_number == tp.line_number &&
                                    field.cml_number == tp.cml_number &&
                                    field.tp_number == tp.tp_number)
                                    .ExecuteDeleteAsync();

                db.SaveChanges();

                return Ok(200);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }
    }
}
