﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using PipeAPI.Models;

namespace PipeAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InfoController : ControllerBase
    {
        private DbCoreContext db = new DbCoreContext();

        [HttpGet]
        public IActionResult GetInfo() 
        {
            List<Info> infos = db.Info.ToList();
            return Ok(infos);
        }

        [HttpPost]
        [Route("[action]")]
        public IActionResult GetInfoByLineNumber([FromBody] Info info)
        {
            List<Info> infos = db.Info.Where(field => field.line_number == info.line_number).ToList();
            return Ok(infos);
        }

        [HttpPost]
        public IActionResult PostInfo([FromBody]Info info) 
        {
            if (info.line_number.IsNullOrEmpty())
            {
                return BadRequest();
            }

            try
            {
                Info infoModel = new Info();
                infoModel.line_number = info.line_number                                        ;
                infoModel.location = info.location;
                infoModel.from = info.from;
                infoModel.to = info.to;
                infoModel.drawing_number = info.drawing_number;
                infoModel.service = info.service;
                infoModel.material = info.material;
                infoModel.inservice_date = info.inservice_date;
                infoModel.pipe_size = info.pipe_size;
                infoModel.original_thickness = info.original_thickness;
                infoModel.stress = info.stress;
                infoModel.joint_efficiency = info.joint_efficiency;
                infoModel.ca = info.ca;
                infoModel.design_life = info.design_life;
                infoModel.design_pressure = info.design_pressure;
                infoModel.operating_pressure = info.operating_pressure;
                infoModel.design_temperature = info.design_temperature;
                infoModel.operating_temperature = info.operating_temperature;

                db.Info.Add(infoModel);
                db.SaveChanges();

                return Ok(200);
            }
            catch (Exception ex)
            { 
                return BadRequest(ex);
            }

        }

        [HttpPatch]
        public IActionResult PatchInfo([FromBody]Info info)
        {
            try
            {
                Info infoModel = db.Info.FirstOrDefault(field => field.line_number == info.line_number);
                
                infoModel.location = info.location;
                infoModel.from = info.from;
                infoModel.to = info.to;
                infoModel.drawing_number = info.drawing_number;
                infoModel.service = info.service;
                infoModel.material = info.material;
                infoModel.inservice_date = info.inservice_date;
                infoModel.pipe_size = info.pipe_size;
                infoModel.original_thickness = info.original_thickness;
                infoModel.stress = info.stress;
                infoModel.joint_efficiency = info.joint_efficiency;
                infoModel.ca = info.ca;
                infoModel.design_life = info.design_life;
                infoModel.design_pressure = info.design_pressure;
                infoModel.operating_pressure = info.operating_pressure;
                infoModel.design_temperature = info.design_temperature;
                infoModel.operating_temperature = info.operating_temperature;

                db.SaveChanges();

                return Ok(200);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }

        [HttpDelete]
        public async Task<IActionResult> DeleteInfo([FromBody] Info info)
        {
            try
            {
                if(db.Thickness.Where((field => field.line_number == info.line_number)).Count() > 0)
                {
                    await db.Thickness.Where(field => field.line_number == info.line_number).ExecuteDeleteAsync();
                }
                if (db.TestPoint.Where((field => field.line_number == info.line_number)).Count() > 0)
                {
                    await db.TestPoint.Where(field => field.line_number == info.line_number).ExecuteDeleteAsync();
                }
                if (db.Cml.Where((field => field.line_number == info.line_number)).Count() > 0)
                {
                    await db.Cml.Where(field => field.line_number == info.line_number).ExecuteDeleteAsync();
                }
                await db.Info.Where(field => field.line_number == info.line_number).ExecuteDeleteAsync();

                db.SaveChanges();

                return Ok(200);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }
    }
}
