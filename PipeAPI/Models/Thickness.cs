﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;

namespace PipeAPI.Models
{
    [PrimaryKey(nameof(line_number), nameof(cml_number), nameof(tp_number), nameof(inspection_date))]
    public class Thickness
    {
        [Column(Order = 0)]
        public string line_number { get; set; }
        [Column(Order = 1)]
        public int cml_number { get; set; }
        [Column(Order = 2)]
        public int tp_number { get; set; }
        [Column(Order = 3)]
        public DateOnly inspection_date { get; set; }
        public double? actual_thickness { get; set; }
    }
}
