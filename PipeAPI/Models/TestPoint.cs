﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;

namespace PipeAPI.Models
{
    [PrimaryKey(nameof(line_number), nameof(cml_number), nameof(tp_number))]
    public class TestPoint
    {
        [Column(Order = 0)]
        public string line_number { get; set; }
        [Column(Order = 1)]
        public int cml_number { get; set; }
        [Column(Order = 2)]
        public int tp_number { get; set; }
        public int? tp_description { get; set; }
        public string? note { get; set; }
    }
}
