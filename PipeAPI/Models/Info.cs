﻿using System.ComponentModel.DataAnnotations;

namespace PipeAPI.Models
{
    public class Info
    {
        [Key]
        public string line_number { get; set; }
        public string? location { get; set; }
        public string? from { get; set; }
        public string? to { get; set; }
        public string? drawing_number { get; set; }
        public string? service { get; set; }
        public string? material { get; set; }
        public DateOnly inservice_date { get; set; }
        public double? pipe_size { get; set; }
        public double? original_thickness { get; set; }
        public double? stress { get; set; }
        public double? joint_efficiency { get; set; }
        public double? ca { get; set; }
        public double? design_life { get; set; }
        public double? design_pressure { get; set; }
        public double? operating_pressure { get; set; }
        public double? design_temperature { get; set; }
        public double? operating_temperature { get; set; }
    }
}
