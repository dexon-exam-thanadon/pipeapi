﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PipeAPI.Models
{
    [PrimaryKey(nameof(line_number), nameof(cml_number))]
    public class Cml
    {
        [Column(Order =0)]
        public string line_number { get; set; }
        [Column(Order = 1)]
        public int cml_number { get; set; }
        public string? cml_description { get; set; }
        public double? actual_outside_diameter { get; set; }
        public double? design_thickness { get; set; }
        public double? structural_thickness { get; set; }
        public double? required_thickness { get; set; }
    }
}
