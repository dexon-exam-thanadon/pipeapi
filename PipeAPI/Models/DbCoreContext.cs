﻿using Microsoft.EntityFrameworkCore;

namespace PipeAPI.Models
{
    public class DbCoreContext : DbContext
    {
        public virtual DbSet<Info> Info { get; set; }
        public virtual DbSet<Cml> Cml { get; set; }
        public virtual DbSet<TestPoint> TestPoint { get; set; }
        public virtual DbSet<Thickness> Thickness { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured) 
            {
                optionsBuilder.UseSqlServer(@"Data Source=LAPTOP-JRUTLI2M;Initial Catalog=PIPING;Integrated Security=True;Trust Server Certificate=True");
            }
        }
    }
}
